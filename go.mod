module simple-kafka-producer

go 1.13

require (
	github.com/confluentinc/confluent-kafka-go v1.3.0
	github.com/spf13/viper v1.6.2
)
