package main

import (
	"fmt"
	"sync"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {

	appName := "go-parse-logs"
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/etc/" + appName)
	viper.AddConfigPath("$HOME/." + appName)
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Unable to load config file %s \n", err))
	}

	fmt.Print(viper.Get("app"))
	bootstrapServer := viper.Get("app.kafka.bootstrap-server")
	fmt.Print(bootstrapServer)

	topic := viper.GetString("app.kafka.topic")
	fmt.Print(topic)

	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": bootstrapServer})

	defer p.Close()

	var wg sync.WaitGroup
	go func() {
		for e := range p.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					fmt.Printf("Delivery failed: %v\n", ev.TopicPartition)
				} else {
					fmt.Printf("Delivered message to %v\n", ev.TopicPartition)
				}
			}
		}
	}()

	for _, word := range []string{"Welcome", "to", "the", "kafka", "golang", "client"} {

		p.Produce(&kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
			Value:          []byte(word)}, nil)

	}

	p.Flush(15 * 1000)
	wg.Wait()
}
